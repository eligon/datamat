#!/usr/bin/env python

class test1(dict):
    """
    Allows one to get values from a dictionary by passing multiple
    keys, e.g., foo=test({"a":1,"b":2})=foo['a','b'].  Whatever the
    number of items, always returns the items as members of the test1 class.
    """

    def __getitem__(self,x):
        if len(x)==1:
            return test1({x:dict.__getitem__(self,x)})
        else:
            d=test1({})
            for y in x:
                d.update(self[y])

            return d

class test2(test1): 
    """
    Inherit multikey referencing from test1; add id attr.
    """
    
    def __init__(self,d,id=None):
        test1.__init__(self,d)
        self.id=id

class test3(dict):
    """
    Inherit multi-key referencing from test1, but this is dict of test2, 
    with keys equal to the id attribute of test2.
    """

    def __init__(self,L,label='Like a dataset'):
        """
        L is a sequence of test2 objects.
        """
        dict.__init__(self)
        for l in L:
            dict.update(self,{l.id:test2(l,l.id)})
        
        self.label=label
        

    def __getitem__(self,x):
        if len(x)==1:
            return test3({x:dict.__getitem__(self,x)})
        else:
            d=test3({})
            for y in x:
                dict.__setitem__(d,y.id,y)

            return d


def regression_tests():
    t1=test1({"a":1,"b":2})
    assert t1['a','b']==t1[('a','b')]  # Can pass a tuple or just list
    assert t1['a']==test1({'a':1})
    t11=test1({(0,):1,(1,):2})
    assert t11[(0,)]==test1({(0,):1})
    print t11[(0,),(1,)]

    t2=test2({"a":3,"b":4},(0,))
    assert t2.id==(0,)
    assert t2['a','b']==t2[('a','b')]  # Can pass a tuple or just list
    assert t2['a']==test1({'a':3})

    t2a=test2({"a":1,"b":2},(0,))
    t2b=test2({"a":3,"b":4},(1,))
    t3=test3((t2a,t2b))
    assert len(t3)==2
    print type(t3)
    print t3[(0,)]
    print t3[(0,),(1,)]

    print "Regression tests okay!"

regression_tests()

