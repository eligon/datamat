#!/usr/bin/env python

import sys
sys.path.append('../utils')

from Dataset import dataset
from Dataset.PyDTA import Reader

base95=Reader(file('BasesECV/base1995c.dta'))
base98=Reader(file('BasesECV/base1998.dta'))
base99=Reader(file('BasesECV/base1999.dta'))
base06=Reader(file('BasesECV/base2006.dta'))

d=dataset()
for obs in base95.dataset(as_dict=True):
    d.append(obs)



if __name__ == "__main__":
    sys.exit(main())
