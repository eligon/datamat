#!/usr/bin/env python

import numpy
import sys
import getopt
import os

def readcsv(fn,delim="\t",miss=["."]):
    import csv

    f=open(fn,"rb")
        
    reader = csv.DictReader(f, delimiter=delim, quotechar='"')

    d=[]
    for row in reader:
        d.append(row)

    return to_numbers(d,miss)

def write_mfile(d):

    allkeys=set([])

    for s in d:
        allkeys = allkeys | set(s.keys())
        
    for k in [s for s in allkeys if s]:  # "if s" clause eliminates empties, None
        try:
            for line in d:  # Need a first pass to look for non-floats
                if line[k]: ['%g' % float(line[k])]

            print '%s = [' % k,
            i=0
            for line in d:  # Now a second pass to actually print
                if line[k]: # if not missing
                    if (float(line[k])-int(line[k])):
                        print '%f' % float(line[k]),
                    else:
                        print '%d' % int(line[k]),
                else: print 'NaN',
                i+=1
                if i==len(d):
                    print '];'
                else: print ',',
        except ValueError:
            print "%s = {'%s'" % (k,d[0][k]),
            for line in d[1:]:
                print ",'%s'" % line[k],
            print '};'

def to_numbers(d,miss):
    allkeys=set([])

    for s in d:
        allkeys = allkeys | set(s.keys())
        
    for k in [s for s in allkeys if s]: # "if s" clause eliminates empties, None

        try:
            for line in d:  # Need a first pass to look for non-floats
                if line[k] in miss:
                    line[k]=None
                if line[k]: ['%g' % float(line[k])]

            i=0
            for line in d:  # Now a second pass to actually change assignment
                if line[k]: # if not missing
                    line[k]=float(line[k])
                else: line[k]=None

        except ValueError: pass
    return d

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "d:h", ["delim","help"])
        except getopt.error, msg:
            raise Usage(msg)

        delim="\t"
        for o in opts:
            if o[0]=='-d': delim=o[1]

        d=readcsv(args[0],delim=delim)

        write_mfile(d)

    except Usage, err:
            print >>sys.stderr, err.msg
            print >>sys.stderr, "for help use --help"
            return 2

if __name__ == "__main__":
    sys.exit(main())
