#!/usr/bin/env python

"""
Let $i\in\mbox{ID}$, with $\mbox{ID}$ a sequence of unique tuples.
Let $j\in\mbox{Names}$, with $\mbox{Names}$ a sequence of unique
strings.

Denote an \emph{observation} by $o_i$.  Any observation $o_i$ has data
$x_{ij}$, saved as a dictionary ${j:x_{ij}}$, and an id i.

A dataset d is a dict of observations ${i:o_i}$, but with unusual
semantics:

Semantics:

  d[i] returns a dataset consisting of a single observation $o_i$.

  d[i_1,...,i_n] returns a dataset consisting of the observations
  $(o_{i_1},\dots,(o_{i_n})].

  d[j] returns a list [x_{ij} for o_i[j] in d] (i.e., values of
  variable v for all observations).

Ethan Ligon                                       November 2007
"""

import sys
import getopt
import os
import numpy
import readcsv
#import tables
import shelve
import la


class observation(dict):
    """
    Let o_i be an observation.  Any observation o_i has
    data x_ij, saved as a dictionary {j:x_ij}, and an id i.

    >>> o_0=observation({"a":1,"b":2},1)
    """
    def __init__(self,d,id=None):
        dict.__init__(self,d)
        if type(id)==type((1,)):
            self.id=id
        else: self.id=(id,)
        
    def __repr__(self):
        idstr=''
        if self.id:
            idstr=tuple.__repr__(self.id)
        iddict=dict.__repr__(self)
        return idstr+": "+iddict

    def __getvarnames(self):
        return self.keys()

    def __getdata(self):
        return self.values()

    varnames=property(__getvarnames, doc="Names of variables")
    data=property(__getdata, doc="Observation data")

    def rename(self,k1,k2):
        """
        Rename variable k1 to k2.
        """
        self.update({k2:self.pop(k1)})


class dataset(dict):
    """
    A dictionary of observations.
    """

    def __init__(self,L=None):

        if L:
            i=0
            for obs in L:
                try:
                    self[obs.id]=obs
                except AttributeError:
                    self[(i,)]=obs
                    i+=1

#        dict.__init__(self,L)

    def __getitem__(self,*items):
        """
        Allows one to get multiple observations via indexing.

        For example self[(101,),(102,)] returns a dataset with two
        observations indexed by the tuples (101,) and (102,).
        """

        d=dataset()
        for item in items:
            print item
            try:
                d.append(dataset([dict.__getitem__(self,item)]))
            except KeyError: pass # If key doesn't exist, don't add to d.

        return d


    def append(self,d): 

        try:
            d.id  # An attribute of an observation
            dict.update(self,d)
        except AttributeError:
            for obs in d.itervalues():
                if self.has_key(obs.id):
                    raise KeyError, \
                        "Observation with id %s already exists." % str(obs.id)
                else:
                    self.append(obs)

    def nextid(self): 
        s=self.iterkeys()
        while True:
            yield s.next()

    def ids(self):
        """List of ids of observations in dataset.

        >>> (1,) in D.ids()
        True
        """

        return [i for i in self.nextid()]
        
    def getvariables(self,vs):

        assert type(vs)==type((1,))

        variables=[]
        for i in range(len(vs)):
            variables.append([q[vs[i]] for q in self.itervalues()])

        return (variables,self.keys())

    def setvariables(self,variables,ids,vals): 

        for i in xrange(len(ids)):
            for j in xrange(len(variables)):
                self[ids[i]][variables[j]]=vals[i][j]

    def matrix(self,vs,missing=numpy.nan):
        """
        Given a tuple of variable names vs, return a matrix.
        """

        for i in range(len(vs)):
            col=[q[vs[i]] for q in self.itervalues()]
            
            # Speed this up
            for j in xrange(len(col)):
                if not numpy.isreal(col[j]):
                    col[j]=missing

            col=numpy.matrix(col,dtype=float).T

            if i:
                x=numpy.c_[x,col]
            else: x=col

        return numpy.reshape(x,(len(self),len(vs)))

    def array(self,vs,missing=numpy.nan,dtype=None):
        """
        Given a tuple of variable names vs, return an array.
        """
        for i in range(len(vs)):
            col=[q[vs[i]] for q in self.itervalues()]
            
            for j in xrange(len(col)):
                if col[j]==None:
                    col[j]=missing

            if dtype:
                col=numpy.array(col,dtype=dtype)
            else:
                col=numpy.array(col)

            if i:
                x=numpy.c_[x,col]
            else: x=col

        # Make sure x has two dimensions
        return x.reshape((x.shape[0],len(vs))) 
                

    def larry(self,vs,missing=numpy.nan):
        """
        Given a tuple of variable names vs, return a labelled array.
        """

        for i in range(len(vs)):
            col=[q[vs[i]] for q in self.itervalues()]
            
            # Speed this up
            for j in xrange(len(col)):
                if not numpy.isreal(col[j]):
                    col[j]=missing

            col=numpy.array(col,dtype=float).T

            if i:
                x=numpy.c_[x,col]
            else: x=col

        x=x.reshape((x.shape[0],len(vs))) # Make sure x has two dimensions

        return la.larry(x,[self.ids(),list(vs)])

    def varnames(self):
        return self.itervalues().next().keys()

    def varvalues(self,var):
        """
        Return realized values of variable var.
        """

        seen=[]
        for obs in self.itervalues():  # Devious clever hack:
            [c for c in [obs[var]] if not (c in seen or seen.append(c))]

        return seen

    def tabulate(self,var,as_nested_dicts=False):
        """
        Return dictionary of realized values of var with frequencies.
        If var is a tuple of variable names, do the right thing by
        returning a dictionary with corresponding tuples of values as
        keys.
        """
        if not type(var)==type((1,)): # var is not tuple
            var=(var,)

        freq={}

        if as_nested_dicts:
            for obs in self.itervalues():
                d={}
                for v in var:
                    freq[obs[v]]
        else:
            for obs in self.itervalues():
                key=tuple([obs[v] for v in var])
                try:
                    freq[key] += 1
                except KeyError:
                    freq[key] = 1

        return freq
                

    def rename(self,k1,k2):
        """
        Rename variable k1 to k2.
        """
        for obs in self.itervalues():
            obs.rename(k1,k2)


    def merge(d1,d2,id): 
        """
        Inputs are two datasets, and an id.

        Returns  a dictionary with a fixed set of dictionary values.
        """

        print "Not yet implemented"

    def save(self,fn):
        """
        Save dataset using shelve module.

        >>> print len(D)
        2
        >>> D.save('my_data.dst')

        BUGS: consider replacing, using pytables?
        """

        f=shelve.open(fn)

        # shelve is a sort of dict, but can only use strings as keys, where
        # dataset uses tuples.  

        # keys
        keystrs=[str(t) for t in self.keys()]

        f.update(zip(keystrs,self.values()))


def load(fn):
    """
    Load dataset using shelve.

    >>> D=load('my_data.dst')
    >>> print len(D)
    2
    """

    f=shelve.open(fn)

    if not f: raise IOError, "No such file"
    else:
        return dataset(f.values())


def getcsv(fn,idstr=None):
    L=readcsv.readcsv(fn,delim=',')

    L=[observation(d) for d in L[1:]]

    if not idstr:
        for t in xrange(len(L)):
           L[t]=observation(L[t],(t,))
    else:
        for t in xrange(len(L)):
           L[t]=observation(L[t],tuple([L[t][idelement] for idelement in idstr]))
 
    return dataset(L)

def getxls(fn,idstr=None,sheet=0):
    """
    Read data from xls spreadsheet; returns a dataset with variable
    names corresponding to column heads.
    """
    import xlrd

    sh=xlrd.open_workbook(fn).sheets()[sheet]

    vnames=sh.row_values(0)
    L=[]
    for i in xrange(1,sh.nrows):
        L.append(observation(zip(vnames,sh.row_values(i))))

    if not idstr:
        for t in xrange(len(L)):
           L[t]=observation(L[t],(t,))
    else:
        for t in xrange(len(L)):
           L[t]=observation(L[t],tuple([L[t][idelement] for idelement in idstr]))
 
    return dataset(L)


if __name__=='__main__':
    import doctest

    # Setup globals for doctests
    o1=observation({"a":1,"b":2},1)
    o2=observation({"a":3,"b":4},2)
    D=dataset([o1,o2])

    doctest.testmod()
    
