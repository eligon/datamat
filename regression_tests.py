#!/usr/bin/env python

from dataset import dataset
from dataset import observation
import unittest

class TestDataset(unittest.TestCase):

    def setUp(self):  # test runner runs "setUp()" prior to each test.
        o1=observation({'a':1,'b':2})
        o2=observation({'a':3,'b':4})
        o3=observation({'a':5,'b':6},(1,))
        o4=observation({'a':3,'b':7},1) # Okay to pass non-tuple
        self.obs=[o1,o2,o3,o4]
        self.dst=dataset(self.obs)

    def testobservation(self):
        print self.obs[1]  # Tests _repr_

    def testconstruction(self):
        print self.dst
        self.assertEqual(len(self.dst),len(self.obs))

    def testindexing(self):
        print self.dst['a','b']

def main():
    suite=unittest.TestLoader().loadTestsFromTestCase(TestDataset)
    unittest.TextTestRunner(verbosity=2).run(suite)

if __name__== '__main__':
    main()
